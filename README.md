# Todo app

## Requirements
Clone TODO app (VueJS)
NodeJS 10+
Agenda

Viết server side cho app todo

Đã có sẵn frontend
Backend sử dụng framework Express
User Stories

Người dùng có thể tạo nhiều todo item
Người dùng có thể mark done 1 hoặc nhiều todo item
Người dùng có thể xem toàn bộ todo (Bao gồm đã hoàn thành và chưa hoàn thành)
Người dùng có thể filter những todo đã hoàn thành
Người dùng có thể filter những todo chưa hoàn thành
Người dùng có thể xóa các todo
Người dùng có thể xóa toàn bộ những todo đã hoàn thành